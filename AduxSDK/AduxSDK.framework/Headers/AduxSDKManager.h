//
//  AduxSDKManager.h
//  AduxSDK
//
//  Created on 17/05/2017.
//  Copyright © 2017. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol AduxSDKDelegate <NSObject>

-(void)didUpdateInfosToServer:(NSDictionary*)dict;
-(void)didFailUpdateInfosToServer:(id)error;
-(void)didSaveInfos:(NSDictionary*)dict;

@end

@interface AduxSDKManager : NSObject

@property (nonatomic,strong) id <AduxSDKDelegate> aduxDelegate;

+(instancetype) instanciate;
-(void) initializeWithDelegate:(id <AduxSDKDelegate>)delegate;
-(void) setIsDebugEnabled:(BOOL)enable;
-(void) enableGeolocation:(BOOL)enable;
-(void) enableNotifications:(BOOL)enable;
-(void) enableCookies:(BOOL)enable;

@end
