//
//  AduxSDK.h
//  AduxSDK
//
//  Created on  17/05/2017.
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AduxSDK.
FOUNDATION_EXPORT double AduxSDKVersionNumber;

//! Project version string for AduxSDK.
FOUNDATION_EXPORT const unsigned char AduxSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AduxSDK/PublicHeader.h>

#import <AduxSDK/AduxSDKManager.h>


