//
//  ViewController.swift
//  AduxSDK-Demo-swift
//
//  Created on 07/06/2017.
//  Copyright © 2017. All rights reserved.
//

import UIKit
import CoreLocation


class ViewController: UIViewController,CLLocationManagerDelegate {
   
    @IBOutlet var locationSwitcher: UISwitch!
    @IBOutlet var notificationsSwitcher: UISwitch!
    @IBOutlet var cookiesSwitcher: UISwitch!
    @IBOutlet var sentInfosTextView: UITextView!
    var appDelegate : AppDelegate!
    var locationManager:CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        appDelegate = UIApplication.shared.delegate as! AppDelegate
    
        var configs: NSDictionary?
        if let path = Bundle.main.path(forResource: "AduxConfigFile", ofType: "plist") {
            configs = NSDictionary(contentsOfFile: path)
        }
        if configs != nil {
            
            locationSwitcher.isOn=configs?.object(forKey: "AduxGeolocationEnabled") as! Bool
            notificationsSwitcher.isOn=configs?["AduxPushNotificationsEnabled"] as! Bool
            cookiesSwitcher.isOn=configs?["AduxCookiesEnabled"] as! Bool
        
        }else{
            locationSwitcher.isOn=false
            notificationsSwitcher.isOn=false
            cookiesSwitcher.isOn=false
        }
        
        // initilaze location manager 
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.didSendInformations),
        name: NSNotification.Name(rawValue: "AduxSendInformationsNotification"),
        object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
    }
    
   
    func didSendInformations(notification: NSNotification)  {
        
        if let json = try? JSONSerialization.data(withJSONObject: notification.object as Any, options: [JSONSerialization.WritingOptions.prettyPrinted]) {
            if let content = String(data: json, encoding: String.Encoding.utf8) {
                // here `content` is the JSON dictionary containing the String
                //print(content)
                sentInfosTextView.text=sentInfosTextView.text+""+content;
                sentInfosTextView.scrollRangeToVisible(NSMakeRange(sentInfosTextView.text.characters.count, 9))
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func locationSwitcherAction(_ sender: Any) {
        appDelegate.manager?.enableGeolocation(locationSwitcher.isOn)
        if locationSwitcher.isOn {
            locationManager.startUpdatingLocation()
        }else{
            locationManager.stopUpdatingLocation()
        }
    }

    @IBAction func notificationsSwitcherAction(_ sender: Any) {
        appDelegate.manager?.enableNotifications(notificationsSwitcher.isOn)
    }
    
    @IBAction func cookiesSwitcherAction(_ sender: Any) {
        appDelegate.manager?.enableCookies(cookiesSwitcher.isOn)
        
    }
}

