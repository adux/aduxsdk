![picture](https://bytebucket.org/adux/aduxsdk/raw/9bd2d99576814bd9a94d270d887f190e22c44c83/Adux.png)

# AduxSDK


## Installation

AduxSDK is available through [CocoaPods](http://cocoapods.org/pods/AduxSDK). To install
it, simply add the following line to your Podfile:

```ruby
	pod "AduxSDK"
```
To run the example project, clone the repo, and run `pod install` from the Example directory first.
If you are not in the correct version, please run `pod update`

##### add a config plist :
To configure, enable and desable the retreived data, you have to add a configuration file as plist in your project appliaction.

```ruby
	AduxConfigFile.plist
```
```ruby
	<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
	<plist version="1.0">
	<dict>
		<key>AduxSendInfosTimer</key>
        <integer>600</integer>
        <key>AduxCollectInfosTimer</key>
        <integer>300</integer>
        <key>AduxAuthorizationKey</key>
        <string>Y2xpZW50U2RrVXNlcjokKiVvUC05YXplcHBs</string>
        <key>AduxCookiesEnabled</key>
        <true/>
        <key>AduxPushNotificationsEnabled</key>
        <false/>
        <key>AduxGeolocationEnabled</key>
        <true/>
        <key>AduxIDFAId</key>
		 <string></string>
		 <key>AduxAnalyticsId</key>
		 <string></string>
	</dict>
	</plist>
```
AduxCollectInfosTimer is the time collecting data (in secondes).
AduxSendInfosTimer is the time sending the collected data to the server (in second).
 
 
Add the folloing imports in your AppDelegate.m

```ruby
	#import <AduxSDK/AduxSDK.h>
```

Objective-c :

```ruby
	- (BOOL)application:(UIApplication *)application 	didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [[AduxSDKManager instanciate] initializeWithDelegate:self];
    
    return YES;
}
```

Swift :

```ruby
	func application(_ application: UIApplication, 	didFinishLaunchingWithOptions launchOptions: 	[UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        AduxSDKManager.instanciate()?.initialize(with: self)
        
        return true
    }
```

To ensure detail information in callbacks, you have to set the delegates in your appdelegate

Objective-c

```ruby
	-(void)didUpdateInfosToServer:(NSDictionary*)dict{
	    NSLog(@"%@", dict);
	}
	
	-(void)didSaveInfos:(NSDictionary *)dict{
	    NSLog(@"%@", dict);
	}
	
	-(void)didFailUpdateInfosToServer:(NSError *)error{
	    NSLog(@"%@", error);
	}

```

Swift :

```ruby
	func didUpdateInfos(toServer dict: [AnyHashable : Any]!) {
        
    }
    func didFailUpdateInfos(toServer error: Any!) {
        
    }
    func didSaveInfos(_ dict: [AnyHashable : Any]!) {
        
    }

```
## Requirements

[App Transport Security](https://developer.apple.com/library/content/documentation/General/Reference/InfoPlistKeyReference/Articles/CocoaKeys.html#//apple_ref/doc/uid/TP40009251-SW33) improves privacy and data integrity by ensuring your app’s network connections employ only industry-standard protocols and ciphers without known weaknesses. This helps instill user trust that your app does not accidentally leak transmitted data to malicious parties.

On December 21st, Apple announced an extended of ATS deadline. Previously, the deadline was January 1, 2017. The new deadline has not yet been announced. Set up the following keys in your project's info.plist :

```ruby
	<key>NSAppTransportSecurity</key>
		<dict>
			<key>NSAllowsArbitraryLoads</key>
			<true/>
	</dict>
```




## Author
Admin Adux

## License

AduxSDK is available under the Adux license. See the LICENSE file for more info.
