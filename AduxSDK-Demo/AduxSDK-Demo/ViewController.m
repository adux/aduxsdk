//
//  ViewController.m
//  AduxSDK-Demo
//
//  Created on 29/05/2017.
//  Copyright © 2017. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "GeoLocationManager.h"
#import <AduxSDK/AduxSDK.h>

#define PLIST_CONFIG_FILE_NAME @"AduxConfigFile"

#define AduxGeolocationEnabled @"AduxGeolocationEnabled"
#define AduxPushNotificationsEnabled @"AduxPushNotificationsEnabled"
#define AduxCookiesEnabled @"AduxCookiesEnabled"

#define SHARED_APPDELEGATE ((AppDelegate*)[UIApplication sharedApplication].delegate)

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSDictionary *configs = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:PLIST_CONFIG_FILE_NAME ofType:@"plist"]];
    
    if (configs!=nil && ![configs isKindOfClass:[NSNull class]]) {
        
        BOOL geolocationEnabled=[[configs valueForKey:AduxGeolocationEnabled] boolValue];
        BOOL notificationEnabled=[[configs valueForKey:AduxPushNotificationsEnabled] boolValue];
        BOOL cookiesEnabled=[[configs valueForKey:AduxCookiesEnabled] boolValue];
        
        [_geolocationSwitcher setOn:geolocationEnabled];
        [_notificationSwitcher setOn:notificationEnabled];
        [_cookiesSwitcher setOn:cookiesEnabled];
        
        if (geolocationEnabled) {
            [[GeoLocationManager sharedInstance] startLocateUserSuccess:^(CLLocation *loc){
                
                
            }Failure:^(NSError *error){
                
            }];
        }
        
        
    }else{
        [_geolocationSwitcher setOn:NO];
        [_notificationSwitcher setOn:NO];
        [_cookiesSwitcher setOn:NO];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                              selector:@selector(didSaveInformations:)
                                                  name:AduxSaveInformationsNotification
                                                object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didSendInformations:)
                                                 name:AduxSendInformationsNotification
                                               object:nil];
    
}
-(void)didSaveInformations:(NSNotification*)notif{
//    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:notif.object options:NSJSONWritingPrettyPrinted error:&error];
//    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//
//    dispatch_sync(dispatch_get_main_queue(), ^{
//        [self.infosTextView setText:[self.infosTextView.text stringByAppendingString:[NSString stringWithFormat:@"\n\n%@",jsonString]]];
//        [self.infosTextView scrollRangeToVisible:NSMakeRange(self.infosTextView.text.length-10, 9)];
//        
//    });
}
-(void)didSendInformations:(NSNotification*)notif{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:notif.object options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [self.infosTextView setText:[self.infosTextView.text stringByAppendingString:[NSString stringWithFormat:@"\n\n%@",jsonString]]];
    [self.infosTextView scrollRangeToVisible:NSMakeRange(self.infosTextView.text.length-10, 9)];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)geolocationSwitcherAction:(id)sender {
    [SHARED_APPDELEGATE.aduxManager enableGeolocation:_geolocationSwitcher.on];
    if(_geolocationSwitcher.on){
        [[GeoLocationManager sharedInstance] startLocateUserSuccess:^(CLLocation *loc){
            
            
        }Failure:^(NSError *error){
            
        }];
    }else{
        [[GeoLocationManager sharedInstance]stopLocateUser];
    }
    
}

- (IBAction)notificationsSwitcherAction:(id)sender {
    [SHARED_APPDELEGATE.aduxManager enableNotifications:_notificationSwitcher.on];
}

- (IBAction)cookiesSwitcherAction:(id)sender {
    [SHARED_APPDELEGATE.aduxManager enableCookies:_cookiesSwitcher.on];
}
@end
