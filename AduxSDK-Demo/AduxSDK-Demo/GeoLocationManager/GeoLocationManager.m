//
//  GeoLocationManager.m
//  DropMyKids
//
//  Created by mac on 17/06/2016.
//  Copyright © 2016 mac. All rights reserved.
//

#import "GeoLocationManager.h"

@implementation GeoLocationManager

+ (GeoLocationManager *)sharedInstance {
    static GeoLocationManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^ {
        instance = [[self alloc]init];
    });
    return instance;
}
- (id)init {
    self = [super init];
    if (!self.locationManager) {
        self.locationManager = [[CLLocationManager alloc]init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.delegate = self;
        [self startLocateUser];
    }
    return self;
}
- (void)startLocateUser {
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
       [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
}
- (void)startLocateUserSuccess:(void (^)(CLLocation* loc))successBlock_ Failure:(void (^)(NSError *err))failureBlock_{
    self.successBlock = successBlock_;
    self.failureBlock = failureBlock_;
    
    isLocated=false;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
}
-(void)stopLocateUser{
    [self.locationManager stopUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Location service failed with error %@", error);
    self.failureBlock(error);
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    self.currentLocation = location;
    if (!isLocated) {
        self.successBlock(location);
        isLocated=true;
    }
}
@end
