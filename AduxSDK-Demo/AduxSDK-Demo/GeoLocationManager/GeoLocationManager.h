//
//  GeoLocationManager.h
//  DropMyKids
//
//  Created on 17/06/2016.
//  Copyright © 2016 . All rights reserved.
//



#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

@interface GeoLocationManager : NSObject <CLLocationManagerDelegate>
{
    BOOL isLocated;
}

+(GeoLocationManager *)sharedInstance;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) void (^successBlock) (CLLocation* loc);
@property (nonatomic, strong) void (^failureBlock)(NSError *err);

- (void)startLocateUserSuccess:(void (^)(CLLocation* loc))successBlock_ Failure:(void (^)(NSError *err))failureBlock_;

-(void)stopLocateUser;

@end
