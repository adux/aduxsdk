//
//  ViewController.h
//  AduxSDK-Demo
//
//  Created on 29/05/2017.
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextView *infosTextView;
@property (strong, nonatomic) IBOutlet UISwitch *geolocationSwitcher;
@property (strong, nonatomic) IBOutlet UISwitch *notificationSwitcher;
@property (strong, nonatomic) IBOutlet UISwitch *cookiesSwitcher;

- (IBAction)geolocationSwitcherAction:(id)sender;
- (IBAction)notificationsSwitcherAction:(id)sender;
- (IBAction)cookiesSwitcherAction:(id)sender;


@end

