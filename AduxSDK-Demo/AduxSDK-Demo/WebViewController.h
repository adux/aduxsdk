//
//  WebViewController.h
//  AduxSDK-Demo
//
//  Created on 30/05/2017.
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)closeButtonAction:(id)sender;
@end
