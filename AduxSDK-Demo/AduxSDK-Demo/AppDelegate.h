//
//  AppDelegate.h
//  AduxSDK-Demo
//
//  Created on 29/05/2017.
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AduxSDK/AduxSDK.h>

extern NSString *const AduxSaveInformationsNotification;
extern NSString *const AduxSendInformationsNotification;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) AduxSDKManager *aduxManager;

@end

